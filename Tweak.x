@interface MFMailComposeController
-(NSArray*)toRecipients;
@end

@interface UIAlertView (EXTERNAL)
@property(assign) NSString* context;
@end

static void $_confirmSend(MFMailComposeController* self) {
  NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.messageui"];
  NSArray* recipients=self.toRecipients;
  NSUInteger nrecipients=recipients.count;
  UIAlertView* alert=[[UIAlertView alloc]
   initWithTitle:[NSString stringWithFormat:@"%@ (%@)",
   NSLocalizedStringFromTableInBundle(@"TO",@"Main",bundle,@"To:"),(nrecipients==1)?
   NSLocalizedStringFromTableInBundle(@"ONE_RECIPIENT",@"Main",bundle,@"1 recipient"):
   [NSString localizedStringWithFormat:NSLocalizedStringFromTableInBundle(@"N_RECIPIENTS",@"Main",
    bundle,@"%@ recipients"),[NSNumber numberWithUnsignedInteger:nrecipients]]]
   message:[recipients componentsJoinedByString:@", "] delegate:self
   cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,
    [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],@"Cancel")
   otherButtonTitles:NSLocalizedStringFromTableInBundle(@"SEND",@"Main",
    bundle,@"Send"),nil];
  alert.context=@"EmptySubjectAlert";
  [alert show];
  [alert release];
}

%hook MFMailComposeController
-(void)_checkForEmptySubject {
  SEL cmd=@selector(_prepareImagesForSend);
  IMP imp=class_replaceMethod(%c(MFMailComposeController),cmd,(IMP)$_confirmSend,"@:");
  %orig;
  class_replaceMethod(%c(MFMailComposeController),cmd,imp,"@:");
}
%end
